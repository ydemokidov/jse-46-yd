package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskUpdateByIndexResponse extends AbstractTaskResponse {

    public TaskUpdateByIndexResponse(@NotNull final TaskDTO taskDTO) {
        super(taskDTO);
    }

    public TaskUpdateByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}

package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.dto.model.ProjectDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable ProjectDTO projectDTO) {
        super(projectDTO);
    }

    public ProjectCreateResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}

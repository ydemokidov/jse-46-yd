package com.t1.yd.tm.api.service;

import com.t1.yd.tm.dto.model.SessionDTO;
import com.t1.yd.tm.dto.model.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    @NotNull
    UserDTO registry(@NotNull String login, @NotNull String password, @NotNull String email);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @Nullable
    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable SessionDTO sessionDTO);

    @Nullable
    UserDTO check(@Nullable String login, @Nullable String password);

}

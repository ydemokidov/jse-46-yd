package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.api.repository.model.ISessionRepository;
import com.t1.yd.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<Session> getClazz() {
        return Session.class;
    }

}

package com.t1.yd.tm.component;

import com.t1.yd.tm.service.DomainService;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        bootstrap.getDomainService().dataBackupSave();
    }

    public void load() {
        if (Files.exists(Paths.get(DomainService.FILE_BACKUP)))
            bootstrap.getDomainService().dataBackupLoad();
    }

}
